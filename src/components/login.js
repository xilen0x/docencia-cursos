import React, { useContext } from 'react';
import { Context } from '../store/appContext';
import { withRouter } from 'react-router-dom';
import '../styles/login.css'


const Login = props => {
    const { store, actions } = useContext(Context);
    return (
        <>
            {
                !!store.errors && (
                    <div className="row">
                        <div className="col-md-12">
                            <div className="alert alert-warning" role="alert">{store.errors.msg} </div>
                        </div>
                    </div>
                )
            }
                <div className="container-fluid color text-center">
                    <form onSubmit={e => actions.login(e, props.history)} className="form-signin">
                        <img className="mb-4" src='./img/LogoUNACH-01.png' alt="" width="150" height="100" />
                        <h1 className="h3 mb-3 font-weight-normal">Ingrese su correo y contraseña</h1>
                        <label for="inputEmail" className="sr-only">Email address</label>
                        <input type="email" className="form-control" id="email" name="email"
                            value={store.email}
                            onChange={actions.handleChange} placeholder="Email address" autofocus />
                        <label for="inputPassword" className="sr-only">Password</label>
                        <input type="password" className="form-control" id="password" name="password"
                            value={store.password}
                            onChange={actions.handleChange} placeholder="Password" />

                        <div className="checkbox mb-3">
                            <label><input type="checkbox" value="remember-me" /> Recordarme</label>
                        </div>
                        <button className="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
                        <small className="mt-5 mb-3">&copy; Carlos Astorga2020</small>
                    </form>
                </div>
        </>
    )
}

export default withRouter(Login);