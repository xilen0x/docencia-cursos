import React from 'react';
import '../styles/dashboard.css'

const Dashboard = props => {
  return (
    <>
      <div className="container-fluid">
        <div className="row ml-5">
          <div className="alert alert-success" role="alert">
            <h4 className="alert-heading">Registro.</h4>
            <p>En este formulario podrás registrar los nuevos cursos y/o talleres que el académico haya realizado.</p>
              </div>
        </div>
        <div className="row">
          <div className="col-md-5 primeracol">
            <form>
              <div className="row">
                <div className="col colemail">
                  <div className="form-group">
                    <label for="email">Email del académico</label>
                    <input type="email" className="form-control" id="email" />
                    <small id="emailHelp" className="form-text text-muted">Ingrese el email institucional.</small>
                    <button type="submit" className="btn btn-success text-right">Buscar</button>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <div className="form-group">
                    <label for="nombre">Nombre del académico</label>
                    <input type="text" className="form-control" id="nombre" disabled />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <div className="form-group">
                    <label for="curso">Curso o Taller realizado</label>
                    <input type="text" className="form-control" id="curso" disabled />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-2">
                  <div className="form-group">
                    <label for="horas">Horas</label>
                    <input type="number" className="form-control" id="horas" disabled />
                  </div>
                </div>
                <div className="col-md-4">
                  <label>Modalidad</label>
                  <select class="custom-select" disabled>
                    <option selected>Seleccione</option>
                    <option value="1">Presencial</option>
                    <option value="2">Remoto</option>
                    <option value="3">Mixto</option>
                  </select>
                </div>
              </div>
              <div className="row">
                <div className="col-md-2">
                  <div className="form-group">
                    <label for="nota">Nota obtenida</label>
                    <input type="number" className="form-control" id="nota" disabled />
                  </div>
                </div>
                <div className="col btnguardar">
                  <button type="submit" className="btn btn-primary btn-block" disabled>Guardar</button>
                </div>
              </div>
            </form>
          </div>
          <div className="col">
            <table class="table table-striped">
              <thead className="thead">
                <tr>
                  <th scope="col">Nombre Académico</th>
                  <th scope="col">Curso/Taller</th>
                  <th scope="col">Horas</th>
                  <th scope="col">Nota</th>
                  <th scope="col">Modalidad</th>
                  <th scope="col">Estado</th>
                  <th scope="col">Certificado</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">Juan Pérez</th>
                  <td>Red Hat System Administration I</td>
                  <td>40</td>
                  <td>100</td>
                  <td>Online</td>
                  <td>Aprobado</td>
                  <td><i class="far fa-3x fa-file-pdf"></i></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  )
}

export default Dashboard;