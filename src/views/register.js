import React, { useContext, useEffect } from 'react';
import { Context } from '../store/appContext';
import { Link } from 'react-router-dom';
import '../styles/register.css'
import Select from '@material-ui/core/Select';


const Register = props => {
    const { store, actions } = useContext(Context)
    return (
        <>
            {
                !!store.errors && (
                    <div className="row">
                        <div className="col-md-12">
                            <div className="alert alert-warning" role="alert">{store.errors.msg} </div>
                        </div>
                    </div>
                )
            }
            <div className="container-fluid mt-5">
                <br />  <p className="text-center">Bienvenido!, Ingrese los siguientes datos para su registro:</p>
                <hr />
                <div className="row justify-content-center">
                    <div className="col-md-6 col-sm-10">
                        <div className="card">
                            <header className="card-header text-white">
                                <h4 className="card-title mt-2">Crear nuevo usuario</h4>
                            </header>
                            <article className="card-body">
                                <form onSubmit={e => actions.register(e, props.history)}>
                                    <div className="row">
                                        <div className="col-md-8 form-group">
                                            <label htmlFor="avatar">Avatar</label>
                                            <input type="file" className="form-control" id="avatar" name="avatar"
                                                onChange={actions.handleChangeFile} />
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col-md-8 form-group">
                                            <label>Nombre </label>
                                            <input type="text" className="form-control" id="nombre" name="nombre" value={store.nombre} onChange={actions.handleChange} />
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col-md-8 form-group">
                                            <label>Apellido</label>
                                            <input type="text" className="form-control" id="apellido" name="apellido" value={store.apellido} onChange={actions.handleChange} />
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col-md-8 form-group">
                                            <label>Email</label>
                                            <input type="email" className="form-control" id="email" name="email" value={store.email} onChange={actions.handleChange} />
                                            <small className="form-text text-muted">Debe ingresar un email válido.</small>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col-md-8 form-group">
                                            <label>Crear contraseña</label>
                                            <input type="password" className="form-control" id="password" name="password" value={store.password} onChange={actions.handleChange} />
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col-md-8 form-group">
                                            <button type="submit" className="btn btn-primary"> Registrarse </button>
                                        </div>
                                        <small className="text-muted">Al hacer clic en el botón 'Registrarse', confirma que acepta nuestros términos de uso y política de privacidad.</small>
                                    </div>
                                </form>
                            </article>
                            <div className="border-top card-body text-center">Ya posse una cuenta? <Link to="/login">Ingresar</Link></div>
                        </div>
                    </div>

                </div>


            </div>

        </>
    )
}

export default Register;