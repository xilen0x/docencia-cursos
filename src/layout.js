import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './views/home';
import NotFound from './views/notfound';
import Navbar from './components/navbar';
import Footer from './components/footer';
import Register from './views/register';
import Login from './components/login';
import UpdateProfile from './views/update-profile'
import ViewProfile from './views/view-profile'
import Dashboard from './views/dashboard';
import injectContext from './store/appContext';
import ChangePass from './components/changePass';
import './styles/login.css'


const Layout = props => {
     return (
        <BrowserRouter>
            <Navbar />
            <Switch>
                <Route exact path="/dashboard" component={Dashboard} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/update-profile" component={UpdateProfile} />
                <Route exact path="/view-profile" component={ViewProfile} />
                <Route exact path="/change-pass" component={ChangePass} />
                <Route exact path="/" component={Home} />
                <Route component={NotFound} />
            </Switch>
           {/*  <Footer /> */}
        </BrowserRouter>
     )
 }

 export default injectContext(Layout)
